# A PHP + Nginx container

### Configuration

Important paths and files

- Nginx Config is at `/data/nginx.conf`, and is wrapped in a `server` directive
- You can add an initialization script at `/data/init.sh` that will be executed _before_ the nginx and php-fpm are started
- Served files should be mounted at `/var/www/`