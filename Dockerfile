ARG php_version
FROM php:$php_version-fpm-alpine

# Update to latest Alpine packages
RUN apk -U upgrade

# PHP_CPPFLAGS are used by the docker-php-ext-* scripts
ENV PHP_CPPFLAGS="$PHP_CPPFLAGS -std=c++11"

ENV DEV_PACKAGES="freetype-dev libjpeg-turbo-dev libpng-dev libxml2-dev libxslt-dev libzip-dev libgcrypt-dev zlib-dev icu-dev make g++"

ADD https://github.com/just-containers/s6-overlay/releases/download/v3.1.0.1/s6-overlay-noarch.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-noarch.tar.xz
ADD https://github.com/just-containers/s6-overlay/releases/download/v3.1.0.1/s6-overlay-x86_64.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-x86_64.tar.xz

RUN set -xe \
    && apk add --no-cache --update --virtual .service-deps\
        icu nginx shadow busybox-suid openssl\
    && apk add --no-cache --update --virtual .build-deps \
        $DEV_PACKAGES \
        $PHPIZE_DEPS \
    && apk add --no-cache --update --virtual .php-extension-deps \
        libpng libxml2 libxslt libzip\
    && docker-php-ext-install -j$(nproc) pdo_mysql bcmath opcache gd xml soap intl xsl mysqli zip\
    && docker-php-ext-enable mysqli\
    && docker-php-ext-configure gd \
    && docker-php-ext-configure intl \
    && usermod -u 1000 www-data \
    && mkdir -p /run/nginx /var/tmp/nginx \
    && mkdir -p /var/www \ 
    && apk del .build-deps \
    && docker-php-source delete \
    && apk update && apk del \
        autoconf \
        bash \
        binutils \
        expat \
        file \
        g++ \
        gcc \
        gdbm \
        gmp \
        isl22 \
        libatomic \
        libbz2 \
        libc-dev \
        libffi \
        libgcc \
        libgomp \
        libmagic \
        libstdc++ \
        m4 \
        make \
        mpc1 \
        musl-dev \
        perl \
        pkgconf \
        python3 \
        re2c \
        readline \
        sqlite-libs \
        .phpize-deps-configure \
    && rm -rf /tmp/* /usr/local/lib/php/doc/* /var/cache/apk/*

RUN { \
        echo 'opcache.memory_consumption=128'; \
        echo 'opcache.interned_strings_buffer=8'; \
        echo 'opcache.max_accelerated_files=4000'; \
        echo 'opcache.revalidate_freq=2'; \
        echo 'opcache.fast_shutdown=1'; \
        echo 'opcache.enable_cli=1'; \
    } > /usr/local/etc/php/conf.d/php-opocache-cfg.ini

RUN ln -s /usr/local/bin/php /usr/bin/php
COPY nginx.conf /etc/nginx/nginx.conf
COPY nginx-site.conf /etc/nginx/conf.d/default.conf
COPY config/services.d/ /etc/services.d/
COPY config/default-nginx.conf /data/nginx.conf
COPY config/index.php /var/www/index.php
COPY start.sh /etc/cont-init.d/start.sh
RUN chmod +x /etc/cont-init.d/start.sh /etc/services.d/nginx/run /etc/services.d/php-fpm/run /etc/services.d/cron/run
RUN chown -R www-data:www-data /var/www /var/tmp /var/tmp/nginx /var/lib/nginx /var/log/nginx

WORKDIR /var/www/

EXPOSE 80
ENTRYPOINT [ "/init" ]

